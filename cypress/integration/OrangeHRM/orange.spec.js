///<reference types="cypress"/>

describe("Orange HRM Check",()=>{
    it('login test',()=>{
        cy.visit('https://opensource-demo.orangehrmlive.com/');
        cy.get('#txtUsername').type('Admin');
        cy.get('#txtPassword').type('admin123');
        cy.get('#btnLogin').click();
    })
   it('HomePage-directory',()=>{
        cy.get('#menu_directory_viewDirectory > b').click();
        cy.get('#txtUsername').type('Admin');
        cy.get('#txtPassword').type('admin123')
        cy.get('#btnLogin').click();
        cy.get('#searchDirectory_emp_name_empName').type('Odis Adalwin');
        cy.get('#searchDirectory_job_title').select('Engineer').should('have.value','3')
        cy.get('#searchDirectory_location').select('Canada').should('have.value','3,-1')

        cy.get('#searchBtn').click();
    })
    it('HomePage-admin',()=>{
        cy.get('#menu_admin_viewAdminModule > b').click();
        cy.get('#txtUsername').type('Admin');
        cy.get('#txtPassword').type('admin123')
        cy.get('#btnLogin').click();
        cy.get('#searchSystemUser_userName').type('Aaliyah.Haq')
        cy.get('#searchSystemUser_userType').select('ESS')
        cy.get('#searchSystemUser_employeeName_empName').type('Aaliyah.Haq')
        cy.get('#searchSystemUser_status').select('Enabled')
        cy.get('#searchBtn').click();
       
    })    
    it('HomePage-PIM',()=>{
        cy.get('#menu_pim_viewPimModule > b').click();
        cy.get('#txtUsername').type('Admin');
        cy.get('#txtPassword').type('admin123')
        cy.get('#btnLogin').click();
        cy.get('#empsearch_employee_name_empName').type('Odis Adalwin')
        cy.get('#empsearch_id').type('0345')
        cy.get('#empsearch_employee_status').select('Full-Time Permanent')
        cy.get('#empsearch_termination').select('Current Employees Only')
        cy.get('#empsearch_supervisor_name').type('John Smith')
        cy.get('#empsearch_job_title').select('Engineer')
        cy.get('#empsearch_sub_unit').select('Administration')
        cy.get('#searchBtn').click();
        cy.contains('PIM').trigger('mouseover')
        cy.get('#menu_pim_Configuration').invoke('show')
        cy.contains('Custom Fields').click({force:true});
        cy.get('#buttonAdd').click();
        cy.get('#customField_name').type('Blood Type')
        cy.get('#customField_screen').select('Emergency Contacts').should('have.value','emergency')
        cy.get('#customField_type').select('Text or Number').should('have.value','0')
        cy.get('#btnSave').click();
        cy.get('.odd > .check > .checkbox').check() .should('be.checked')  .and('have.value','1')  
        cy.get('#buttonAdd').click();
        cy.get('#dialogDeleteBtn').click({force:true});
          






    })

  
   

})
   